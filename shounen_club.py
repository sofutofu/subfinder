from skimage import img_as_float
from skimage.metrics import structural_similarity as ssim
# from joblib import Parallel, delayed
from pims import Video, process, as_grey
from pandas import DataFrame
from time import perf_counter
from os import path

########### Random functions

def progress_reporter(message, good=True):
    if good:
        icon = "✔️"
    else:
        icon = "❌"
    print(icon+" "+message+" (Elapsed: "+str(round(perf_counter()-start_time,2))+"s)")


########### Set up arguments etc.
import argparse

parser = argparse.ArgumentParser(description = "Identify lyric changes (keyframes) in Shounen Club videos. Expects MP4 video, reencoded to constant fps if needed.")
parser.add_argument("file_path", help = "path to the video")
parser.add_argument("-t", "--threshold",default=.2, help = "similarity threshold to trigger a frame change flag (default 0.2)",type=float)
parser.add_argument("-o","--out_dir","-out",default="", help = "set a different output directory than script location")
parser.add_argument("--off", default=False, help = "don't write the standard output files")
parser.add_argument("-e","--easyocr", action='store_true', help = "use easyocr on the keyframes")

args = parser.parse_args()

start_time = perf_counter()

############ Load frames from video
video = Video(args.file_path)
# Pylance will claim the rest of this code is unreachable but... it's not...

# Crop to expected location of subs based on video dimensions
x_start = round((30/960)*video.frame_shape[1],0)
y_start = round((471/540)*video.frame_shape[0])
x_length = round((50/960)*video.frame_shape[1])
y_length = round((17/540)*video.frame_shape[0])
video = process.crop(video,((y_start,y_length),(x_start,x_length),(0,0))) 

# Greyscale
frame_list = as_grey(video)

# Progress update
progress_reporter("Loaded and processed video")

############ Calculate similarity for neighboring frames
frame_list = img_as_float(frame_list)

# Can't get itertools to do its thing
from itertools import tee

def pairwise(iterable):
    # pairwise('ABCDEFG') --> AB BC CD DE EF FG
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)

def similarize(x,y):
    return ssim(x, y, data_range=y.max() - y.min())

# similarity = Parallel(n_jobs=4)(delayed(similarize)(x,y) for x,y in pairwise(image_list_float))
similarity = [similarize(x,y) for x,y in pairwise(frame_list)]
# Non-parallel is a little slower but not by enough to warrant loading the library, probably

# Progress update
progress_reporter("Calculated similarity")

############# Set up summary table and thresholding
summary = DataFrame(
    {
        "frame": [*range(1,len(similarity)+1)],
        "similarity": similarity
    }
)

thresh = args.threshold
summary["flag"] = ~(summary["similarity"].gt(summary["similarity"].shift(1)-thresh) & summary["similarity"].gt(summary["similarity"].shift(-1)-thresh))
summary.loc[0,"flag"] = False
# Manually set the first frame to not be flagged

# Progress update
print("✔️ Generated flags (Cumulative elapsed: "+str(round(perf_counter()-start_time,2))+"s)")

############# Write summary table, keyframes files

if not args.off:
    try:
        summary.to_csv(path.join(args.out_dir,"output.csv"),index_label=False,index=False)
        # Progress update
        progress_reporter("Wrote output to file")
    except PermissionError:
        progress_reporter("Output.csv not saved due to permissions error. Do you have the old output.csv open in Excel?",good=False)

    # Write Aegisub keyframe file
    try:
        with open(path.join(args.out_dir,"keyframes.txt"),"w") as f:
            f.write("# keyframe format v1\nfps 0\n")

        summary.query('flag==True')["frame"].to_csv(path.join(args.out_dir,"keyframes.txt"),sep="\n",index=False,mode="a",header=False)
        # Progress update
        progress_reporter("Wrote keyframes to file")
    except:
        progress_reporter("Keyframe file not saved.", good=False)

############# EasyOCR the keyframes

if args.easyocr:
    import easyocr
    from skimage import img_as_ubyte

    reader = easyocr.Reader(['ja', 'en'])
    def easyocr_it(frame):
        # See if readtext_batched works
        return "".join([a[1] for a in reader.readtext(img_as_ubyte(frame))])


    easyocr_results = DataFrame(
        {
            "frame": summary.loc[summary["flag"] == True, "frame"]
        }
    )

    # Progress update
    progress_reporter("Starting EasyOCR on keyframes")

    # TO DO: figure out what's throwing this warning VideoStream.seek is deprecated.

    easyocr_results["ocr_result"] = [easyocr_it(frame) for frame in video[easyocr_results["frame"]]]

    from rapidfuzz import fuzz

    easyocr_results["distance"] = [fuzz.ratio(a,b) for a,b in zip(easyocr_results["ocr_result"],easyocr_results["ocr_result"].shift(1,fill_value=""))]

    try:
        easyocr_results.query('distance <= 50').to_csv(path.join(args.out_dir,"easyocr_results.csv"),index_label=False,index=False)
    except PermissionError:
        progress_reporter("EasyOCR output not saved due to permissions error. Do you have the old file open in Excel?", good=False)

    # Progress update
    progress_reporter("Wrote EasyOCR output to files")

    # Get FPS and write ASS file
    from pandas import to_datetime
    easyocr_results["s_time"] = to_datetime((easyocr_results["frame"]/30),unit="s").dt.strftime("%H:%M:%S.%f")
    easyocr_results["e_time"] = easyocr_results["s_time"].shift(-1).ffill()

    easyocr_results["ass"] = easyocr_results.agg("Dialogue: 0,{0[s_time]},{0[e_time]},Everyone,,0,0,0,,{0[ocr_result]}".format, axis=1)

    # Write ASS file
    template=""
    try:
        with open("template.ass","r") as f:
            template = f.read()
    except:
        progress_reporter("Failed to read ASS template. ASS will be generated without styles.", good=False)

    try:
        with open(path.join(args.out_dir,"sub_template.ass"),"w") as f:
            f.write(template)

        easyocr_results["ass"].to_csv(path.join(args.out_dir,"sub_template.ass"),sep="\n",index=False,mode="a",header=False)
        # Progress update
        progress_reporter("Wrote sub template .ass to file")
    except:
        progress_reporter("Sub template .ass not saved.", good=False)