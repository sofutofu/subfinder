# *subfinder*

Tools to improve efficiency in subtitling, particularly in timing subs to match existing on-screen subs during music shows.

I developed these as part of working with [IwaSakuFC](https://iwasakufc.livejournal.com) to sub [Snow Man](https://mentrecording.jp/snowman/) videos.

## shounen_club.py

> Autotimes Shounen Club on-screen lyrics by watching for changes in subtitle text.

```
usage: shounen_club.py [-h] [-t THRESHOLD] [-o OUT_DIR] [--off OFF] [-e] file_path

Identify lyric changes (keyframes) in Shounen Club videos. Expects MP4 video, reencoded to constant fps if needed.

positional arguments:
  file_path             path to the video

optional arguments:
  -h, --help            show this help message and exit
  -t THRESHOLD, --threshold THRESHOLD
                        similarity threshold to trigger a frame change flag (default 0.2)
  -o OUT_DIR, --out_dir OUT_DIR, -out OUT_DIR
                        set a different output directory than script location
  --off OFF             don't write the standard output files
  -e, --easyocr         use easyocr on the keyframes
```